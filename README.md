# # protractor-cucumber-tests
![enter image description here](https://i.ibb.co/Sf9X5Lc/Untitled.png) 
E2E GUI web app test automation framework

Requires to be installed:

 - Node.js
 - Chrome

## After cloning the repo

    npm install
    node_modules/protractor/bin/webdriver-manager update


## Run the tests

    npm run test

## Reporting

This project uses  [cucumber-html-reporter](https://www.npmjs.com/package/cucumber-html-reporter). An HTML report is generated in the root directory after tests are run.

Happy testing  :hibiscus: :octopus:

