"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
const advancedSearchPO_1 = require("../page_objects/advancedSearchPO");
const advancedSearchPO = new advancedSearchPO_1.AdvancedSearchPO();
class AdvancedSearchPF {
    GetStartEndDates() {
        return __awaiter(this, void 0, void 0, function* () {
            const strStartDate = yield protractor_1.browser.executeScript('return arguments[0].value', advancedSearchPO.tfStartDate);
            const arrStartDate = strStartDate.split('/');
            const startDate = new Date(parseInt(arrStartDate[2]), parseInt(arrStartDate[1]) - 1, parseInt(arrStartDate[0]));
            const strEndDate = yield protractor_1.browser.executeScript('return arguments[0].value', advancedSearchPO.tfEndDate);
            const arrEndDate = strEndDate.split('/');
            const endDate = new Date(parseInt(arrEndDate[2]), parseInt(arrEndDate[1]) - 1, parseInt(arrEndDate[0]));
            return { startDate, endDate };
        });
    }
    SetThisWeekendDates() {
        const today = new Date();
        let thisSat = new Date(today.setDate(today.getDate() + (6 - today.getDay())));
        let thisSun = new Date(today.setDate(today.getDate() + 1));
        return { thisSat, thisSun };
    }
    SetNextWeekDates() {
        const { thisSun } = this.SetThisWeekendDates();
        const nextMon = new Date(thisSun.setDate(thisSun.getDate() + 1));
        const nextSun = new Date(thisSun.setDate(thisSun.getDate() + 6));
        return { nextMon, nextSun };
    }
    SetNextWeekendDates() {
        const { nextSun } = this.SetNextWeekDates();
        const nextWeekSun = new Date(nextSun);
        const nextWeekSat = new Date(nextSun.setDate(nextSun.getDate() - 1));
        return { nextWeekSat, nextWeekSun };
    }
    CheckInputDatesIsValidRange(strFromDate, strToDate) {
        return new Date(strFromDate) <= new Date(strToDate);
    }
    SelectDatePicker(strDate) {
        return __awaiter(this, void 0, void 0, function* () {
            let inputDate = new Date(strDate);
            let currDate = new Date(yield advancedSearchPO.lblPickerMonthYear.getText());
            while (true) {
                if (inputDate.getFullYear() < currDate.getFullYear()) {
                    yield protractor_1.browser.executeScript('arguments[0].click()', advancedSearchPO.btnPrevMonth);
                }
                else if (inputDate.getFullYear() > currDate.getFullYear()) {
                    yield protractor_1.browser.executeScript('arguments[0].click()', advancedSearchPO.btnNextMonth);
                }
                else if (inputDate.getMonth() < currDate.getMonth()) {
                    yield protractor_1.browser.executeScript('arguments[0].click()', advancedSearchPO.btnPrevMonth);
                }
                else if (inputDate.getMonth() > currDate.getMonth()) {
                    yield protractor_1.browser.executeScript('arguments[0].click()', advancedSearchPO.btnNextMonth);
                }
                else
                    break;
            }
        });
    }
    SelectByMainCategory(category) {
        return __awaiter(this, void 0, void 0, function* () {
            const expVal = category.split(',').map(s => s.trim());
            for (let i = 0; i < expVal.length; i++) {
                let match = false;
                const total = yield advancedSearchPO.lblCatMain.count();
                for (let j = 0; j < total; j++) {
                    const actVal = yield protractor_1.browser.executeScript('return arguments[0].innerText;', yield advancedSearchPO.lblCatMain.get(j))
                        .then(s => s.toString().split(/\r?\n/).map(x => x.trim())[0]);
                    if (actVal.localeCompare(expVal[i], 'en', { sensitivity: 'base' }) === 0) {
                        yield protractor_1.browser.executeScript('arguments[0].click()', advancedSearchPO.lblCatMain.get(j));
                        if ((yield advancedSearchPO.lblCatMain.get(j).getCssValue('color')) !== 'rgba(244, 133, 0, 1)') {
                            yield console.log(`${expVal[i]} is not selected`);
                            return false;
                        }
                        match = true;
                        break;
                    }
                }
                if (match === false) {
                    yield console.log(`No matching result for ${expVal[i]}`);
                    return false;
                }
            }
            return true;
        });
    }
    SelectBySubCategory(category) {
        return __awaiter(this, void 0, void 0, function* () {
            const expVal = category.split('/').map(s => s.trim());
            for (let i = 0; i < expVal.length; i++) {
                let match = false;
                const total = yield advancedSearchPO.lblCatSub.count();
                for (let j = 0; j < total; j++) {
                    const actVal = yield protractor_1.browser.executeScript('return arguments[0].innerText;', yield advancedSearchPO.lblCatSub.get(j));
                    if (actVal.includes(expVal[i])) {
                        yield protractor_1.browser.executeScript('arguments[0].click()', advancedSearchPO.lblCatSub.get(j));
                        if ((yield advancedSearchPO.lblCatSub.get(j).getCssValue('color')) !== 'rgba(244, 133, 0, 1)') {
                            yield console.log(`${expVal[i]} is not selected`);
                            return false;
                        }
                        match = true;
                        break;
                    }
                }
                if (match === false) {
                    yield console.log(`No matching result for ${expVal[i]}`);
                    return false;
                }
            }
            return true;
        });
    }
    SelectByMultiIntel(intel) {
        return __awaiter(this, void 0, void 0, function* () {
            const expVal = intel.split(',').map(s => s.trim());
            for (let i = 0; i < expVal.length; i++) {
                let match = false;
                const total = yield advancedSearchPO.lblMultiIntel.count();
                for (let j = 0; j < total; j++) {
                    const actVal = yield protractor_1.browser.executeScript("return arguments[0].innerText", advancedSearchPO.lblMultiIntel.get(j))
                        .then(s => s.toString().trim());
                    if (actVal.localeCompare(expVal[i], 'en', { sensitivity: 'base' }) === 0) {
                        yield protractor_1.browser.executeScript('arguments[0].click()', advancedSearchPO.lblMultiIntel.get(j));
                        if ((yield advancedSearchPO.lblMultiIntel.get(j).getCssValue('color')) !== 'rgba(244, 133, 0, 1)') {
                            yield console.log(`${expVal[i]} is not selected`);
                            return false;
                        }
                        match = true;
                        break;
                    }
                }
                if (match === false) {
                    yield console.log(`No matching result for ${expVal[i]}`);
                    return false;
                }
            }
            return true;
        });
    }
}
exports.AdvancedSearchPF = AdvancedSearchPF;
