import { browser } from "protractor";
import { AdvancedSearchPO } from "../page_objects/advancedSearchPO";
import { protractor } from "protractor/built/ptor";

const advancedSearchPO = new AdvancedSearchPO();

export class AdvancedSearchPF {

    async GetStartEndDates() {
        const strStartDate: string = await browser.executeScript('return arguments[0].value', advancedSearchPO.tfStartDate);
        const arrStartDate = strStartDate.split('/');
        const startDate = new Date(parseInt(arrStartDate[2]), parseInt(arrStartDate[1]) - 1, parseInt(arrStartDate[0]));

        const strEndDate: string = await browser.executeScript('return arguments[0].value', advancedSearchPO.tfEndDate);
        const arrEndDate = strEndDate.split('/');
        const endDate = new Date(parseInt(arrEndDate[2]), parseInt(arrEndDate[1]) - 1, parseInt(arrEndDate[0]));

        return { startDate, endDate }
    }

    SetThisWeekendDates() {
        const today = new Date();
        let thisSat = new Date(today.setDate(today.getDate() + (6 - today.getDay())));
        let thisSun = new Date(today.setDate(today.getDate() + 1));
        return { thisSat, thisSun }
    }

    SetNextWeekDates() {
        const { thisSun } = this.SetThisWeekendDates();
        const nextMon = new Date(thisSun.setDate(thisSun.getDate() + 1));
        const nextSun = new Date(thisSun.setDate(thisSun.getDate() + 6));
        return { nextMon, nextSun };
    }

    SetNextWeekendDates() {
        const { nextSun } = this.SetNextWeekDates();
        const nextWeekSun = new Date(nextSun);
        const nextWeekSat = new Date(nextSun.setDate(nextSun.getDate() - 1));
        return { nextWeekSat, nextWeekSun };
    }

    CheckInputDatesIsValidRange(strFromDate: string, strToDate: string) {
        return new Date(strFromDate) <= new Date(strToDate);
    }

    async SelectDatePicker(strDate: string) {
        let inputDate = new Date(strDate);
        let currDate = new Date(await advancedSearchPO.lblPickerMonthYear.getText());
        while (true) {
            if (inputDate.getFullYear() < currDate.getFullYear()) {
                await browser.executeScript('arguments[0].click()', advancedSearchPO.btnPrevMonth);
            }
            else if (inputDate.getFullYear() > currDate.getFullYear()) {
                await browser.executeScript('arguments[0].click()', advancedSearchPO.btnNextMonth);
            }
            else if (inputDate.getMonth() < currDate.getMonth()) {
                await browser.executeScript('arguments[0].click()', advancedSearchPO.btnPrevMonth);
            }
            else if (inputDate.getMonth() > currDate.getMonth()) {
                await browser.executeScript('arguments[0].click()', advancedSearchPO.btnNextMonth);
            }
            else break;
        }
    }

    async SelectByMainCategory(category: string) {
        const expVal = category.split(',').map(s => s.trim());
        for (let i = 0; i < expVal.length; i++) {
            let match = false;
            const total = await advancedSearchPO.lblCatMain.count();
            for (let j = 0; j < total; j++) {
                const actVal: string = await browser.executeScript('return arguments[0].innerText;', await advancedSearchPO.lblCatMain.get(j))
                    .then(s => s.toString().split(/\r?\n/).map(x => x.trim())[0]);
                if (actVal.localeCompare(expVal[i], 'en', { sensitivity: 'base' }) === 0) {
                    await browser.executeScript('arguments[0].click()', advancedSearchPO.lblCatMain.get(j));
                    if (await advancedSearchPO.lblCatMain.get(j).getCssValue('color') !== 'rgba(244, 133, 0, 1)') {
                        await console.log(`${expVal[i]} is not selected`);
                        return false;
                    }
                    match = true;
                    break;
                }
            }
            if (match === false) {
                await console.log(`No matching result for ${expVal[i]}`);
                return false;
            }
        }
        return true;
    }

    async SelectBySubCategory(category: string) {
        const expVal = category.split('/').map(s => s.trim());
        for (let i = 0; i < expVal.length; i++) {
            let match = false;
            const total = await advancedSearchPO.lblCatSub.count();
            for (let j = 0; j < total; j++) {
                const actVal: string = await browser.executeScript('return arguments[0].innerText;', await advancedSearchPO.lblCatSub.get(j));
                if (actVal.includes(expVal[i])) {
                    await browser.executeScript('arguments[0].click()', advancedSearchPO.lblCatSub.get(j));
                    if (await advancedSearchPO.lblCatSub.get(j).getCssValue('color') !== 'rgba(244, 133, 0, 1)') {
                        await console.log(`${expVal[i]} is not selected`);
                        return false;
                    }
                    match = true;
                    break;
                }
            }
            if (match === false) {
                await console.log(`No matching result for ${expVal[i]}`);
                return false;
            }
        }
        return true;
    }

    async SelectByMultiIntel(intel: string) {
        const expVal = intel.split(',').map(s => s.trim())
        for (let i = 0; i < expVal.length; i++) {
            let match = false;
            const total = await advancedSearchPO.lblMultiIntel.count();
            for (let j = 0; j < total; j++) {
                const actVal: string = await browser.executeScript("return arguments[0].innerText", advancedSearchPO.lblMultiIntel.get(j))
                    .then(s => s.toString().trim())
                if (actVal.localeCompare(expVal[i], 'en', { sensitivity: 'base' }) === 0) {
                    await browser.executeScript('arguments[0].click()', advancedSearchPO.lblMultiIntel.get(j))
                    if (await advancedSearchPO.lblMultiIntel.get(j).getCssValue('color') !== 'rgba(244, 133, 0, 1)') {
                        await console.log(`${expVal[i]} is not selected`)
                        return false;
                    }
                    match = true;
                    break;
                }
            }
            if (match === false) {
                await console.log(`No matching result for ${expVal[i]}`);
                return false;
            }
        }
        return true;
    }
}